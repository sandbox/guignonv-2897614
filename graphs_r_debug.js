/**
 * @file
 * Graphs Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_r_debug = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  render: function (graph_index, rdr_index) {
    var graph = Drupal.settings.graphs.graphs[graph_index];
    var sourcer = Drupal.settings.graphs.sourcers[graph.sourcer];
    var known_nodes = {};

    function addNodesAt(node_id) {
      Drupal.behaviors.graphs.getGraph(
        graph_index,
        graph.sourcer,
        {
          graph_id: graph.id,
          node_id: node_id,
          callback: function (graph_data) {
            console.log(graph_data);
            var regex_node_id = node_id.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
            var $current_graph = $('#graphs_' + graph_index + ' .graphs_r_debug_json:eq(1) .json')
              .html(
                JSON
                  .stringify(sourcer.graph, null, 2)
                  .replace(
                    new RegExp('^(.*\\W)' + regex_node_id + '(\\W.*)$', 'gm'),
                    '<span class="processed-node">$1' + node_id + '$2</span>'
                  )
              );
            var $current_nodes = $('#graphs_' + graph_index + ' .graphs_r_debug_json:eq(2) .json')
              .html(
                JSON
                  .stringify(sourcer.nodes, null, 2)
                  .replace(
                    new RegExp('^([^:\\n\\r]*\\W)' + regex_node_id + '(\\W[^\\}]*\\})', 'gm'),
                    '<span class="processed-node">$1' + node_id + '$2</span>'
                  )
              );
            $('#graphs_' + graph_index + ' .graphs_r_debug_json:eq(3) .json')
              .html(JSON.stringify(graph_data, null, 2));
            var $graph_node_area = $('#graphs_' + graph_index + ' ul')
            $graph_node_area.find('.previous-new-node')
              .removeClass('previous-new-node');
            $graph_node_area.find('.new-node')
              .removeClass('new-node')
              .addClass('previous-new-node');
            graph_data.nodes.forEach(function(node) {
              if (!known_nodes[node.id]) {
                known_nodes[node.id] = true;
                $graph_node_area.append(
                  $("  <li class=\"new-node\">" + node.id + "</li>\n").on('click', function () {
                    $(this).not('.processed-node').addClass('processed-node');
                    addNodesAt(node.id);
                  })
                );
                // Highlight new nodes.
                regex_node_id = node.id.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
                // One current graph data.
                var graph_json_highlight = $current_graph.html();
                graph_json_highlight = graph_json_highlight.replace(
                  new RegExp('^(.*\\W)' + regex_node_id + '(\\W.*)$', 'gm'),
                  '<span class="new-node">$1' + node.id + '$2</span>'
                );
                $current_graph.html(graph_json_highlight);
                // On current nodes.
                var nodes_json_highlight = $current_nodes.html();
                nodes_json_highlight = nodes_json_highlight.replace(
                  new RegExp('^([^:\\n\\r]*\\W)' + regex_node_id + '(\\W[^\\}]*\\})', 'gm'),
                  '<span class="new-node">$1' + node.id + '$2</span>'
                );
                $current_nodes.html(nodes_json_highlight);
              }
            });
          }
        }
      );
    }

    try {
      console.log('Loading graph data using sourcer node function.');
      Drupal.behaviors.graphs.getGraph(
        graph_index,
        graph.sourcer,
        {
          graph_id: graph.id,
          callback: function (graph_data) {
            console.log(graph_data);
            var $graph_area = $('#graphs_' + graph_index)
              .addClass('graphs_r_debug')
              .html(
                "<div class=\"graphs_r_debug_json\"><h3>Initial JSON graph data:</h3>\n<div class=\"json\">"
                + JSON.stringify(graph_data, null, 2)
                + "</div></div>\n<div class=\"graphs_r_debug_json\"><h3>Current JSON graph data:</h3>\n<div class=\"json\">"
                + JSON.stringify(graph_data, null, 2)
                + "</div></div>\n<br class=\"clearfix\"/>\n<div class=\"graphs_r_debug_json\"><h3>Nodes:</h3>\n<div class=\"json\">"
                + JSON.stringify(sourcer.nodes, null, 2)
                + "</div></div>\n<div class=\"graphs_r_debug_json\"><h3>Clicked node sub-graph data:</h3>\n<div class=\"json\"></div></div>\n<br class=\"clearfix\"/>\n<h3>Node list:</h3>\n"
              );
            var $graph_node_area = $("<ul></ul>\n").appendTo($graph_area);
            graph_data.nodes.forEach(function(node) {
              known_nodes[node.id] = true;
              $graph_node_area.append(
                $("  <li>" + node.id + "</li>\n").on('click', function () {
                  $(this).not('.processed-node').addClass('processed-node');
                  addNodesAt(node.id);
                })
              );
            });
          }
        }
      );
    }
    catch (error) {
      graph_data = error.message;
    }
  }
};

}(jQuery));
